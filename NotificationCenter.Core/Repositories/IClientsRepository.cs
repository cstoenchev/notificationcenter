﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NotificationCenter.Models.Domains.Clients;

namespace NotificationCenter.Core.Repositories
{
    /// <summary>
    /// The clients repository interface.
    /// </summary>
    public interface IClientsRepository
    {
        /// <summary>
        /// Gets all clients asynchronous.
        /// </summary>
        Task<IEnumerable<ClientBase>> GetAllAsync();
    }
}
