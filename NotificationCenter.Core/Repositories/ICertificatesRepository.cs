﻿using System.Threading.Tasks;
using NotificationCenter.Models.Domains.Certificates;

namespace NotificationCenter.Core.Repositories
{
    /// <summary>
    /// The certificates repository interface.
    /// </summary>
    public interface ICertificatesRepository
    {
        /// <summary>
        /// Creates the certificate asynchronous.
        /// </summary>
        /// <param name="certificate">The certificate.</param>
        Task<int> CreateAsync(Certificate certificate);

        /// <summary>
        /// Gets the certificate by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        Task<Certificate> GetByIdAsync(int id);
    }
}
