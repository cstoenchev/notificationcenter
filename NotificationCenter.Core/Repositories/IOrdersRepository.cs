﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NotificationCenter.Models.Domains.Orders;

namespace NotificationCenter.Core.Repositories
{
    /// <summary>
    /// The orders repository interface.
    /// </summary>
    public interface IOrdersRepository
    {
        /// <summary>
        /// Gets the orders for user asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        Task<IEnumerable<OrderDetails>> GetForUserAsync(int userId);

        /// <summary>
        /// Gets the order by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        Task<Order> GetByIdAsync(int id);

        /// <summary>
        /// Gets the status by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        Task<OrderStatus> GetStatusByIdAsync(int id);

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="orderId">The order identifier.</param>
        /// <param name="statusId">The status identifier.</param>
        Task<bool> UpdateStatus(int orderId, int statusId);
    }
}
