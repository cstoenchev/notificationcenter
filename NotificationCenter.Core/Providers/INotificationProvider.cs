﻿using System;
using System.Threading.Tasks;
using NotificationCenter.Models.Domains.Orders;

namespace NotificationCenter.Core.Providers
{
    /// <summary>
    /// The notification provider interface.
    /// </summary>
    public interface INotificationProvider
    {
        /// <summary>
        /// Notifies the changed order status asynchronous.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="newStatus">The new status.</param>
        Task NotifyChangedOrderStatusAsync(Order order, OrderStatus newStatus);

        /// <summary>
        /// Notifies the expired certificate asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        Task NotifyExpiredCertificateAsync(int id);

        /// <summary>
        /// Registers the notification for expired certificate asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        Task RegisterNotificationExpiredCertificateAsync(int id, DateTime expiresAt);
    }
}
