﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NotificationCenter.Models.Domains.Clients;

namespace NotificationCenter.Core.Services
{
    /// <summary>
    /// The clients service interface.
    /// </summary>
    public interface IClientsService
    {
        /// <summary>
        /// Gets all clients asynchronous.
        /// </summary>
        Task<IEnumerable<ClientBase>> GetAllAsync();
    }
}
