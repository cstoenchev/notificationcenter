﻿using System.Threading.Tasks;

namespace NotificationCenter.Core.Services
{
    /// <summary>
    /// The users service interface.
    /// </summary>
    public interface IUsersService
    {
        /// <summary>
        /// Gets the user identifier by credentials asynchronous.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        Task<int?> GetUserIdByCredentialsAsync(string userName, string password);

        /// <summary>
        /// Gets the user identifier by user name asynchronous.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        Task<int?> GetUserIdByUserNameAsync(string userName);

        /// <summary>
        /// Gets the user identifier by client identifier asynchronous.
        /// </summary>
        /// <param name="clientId">The client identifier.</param>
        Task<int?> GetUserIdByClientIdAsync(int clientId);
    }
}
