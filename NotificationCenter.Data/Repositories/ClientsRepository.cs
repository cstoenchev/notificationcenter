﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Models.Domains.Clients;

namespace NotificationCenter.Data.Repositories
{
    /// <summary>
    /// The clients repository implementation.
    /// </summary>
    /// <seealso cref="IClientsRepository" />
    public class ClientsRepository : IClientsRepository
    {
        #region Fields

        private readonly IDbConnection _dbConnection;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientsRepository"/> class.
        /// </summary>
        public ClientsRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        #endregion

        #region Public Methods

        /// See <see cref="IClientsRepository"/> for more.
        public Task<IEnumerable<ClientBase>> GetAllAsync()
        {
            Task<IEnumerable<ClientBase>> result = _dbConnection.QueryAsync<ClientBase>("prc_Clients_GetAll", commandType: CommandType.StoredProcedure);

            return result;
        }

        #endregion
    }
}
