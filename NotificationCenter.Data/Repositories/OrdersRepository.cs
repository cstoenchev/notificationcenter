﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Models.Domains.Orders;

namespace NotificationCenter.Data.Repositories
{
    /// <summary>
    /// The orders repository implementation.
    /// </summary>
    /// <seealso cref="IOrdersRepository" />
    public class OrdersRepository : IOrdersRepository
    {
        #region Fields

        private readonly IDbConnection _dbConnection;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersRepository"/> class.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        public OrdersRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        #endregion

        #region Public Methods

        /// See <see cref="IOrdersRepository"/> for more.
        public Task<IEnumerable<OrderDetails>> GetForUserAsync(int userId)
        {
            var inParams = new
            {
                userId
            };

            Task<IEnumerable<OrderDetails>> result = _dbConnection.QueryAsync<OrderDetails>("prc_Orders_GetForUser", inParams, commandType: CommandType.StoredProcedure);

            return result;
        }

        /// See <see cref="IOrdersRepository"/> for more.
        public Task<Order> GetByIdAsync(int id)
        {
            var inParams = new
            {
                id
            };

            Task<Order> result = _dbConnection.QuerySingleOrDefaultAsync<Order>("prc_Orders_GetById", inParams, commandType: CommandType.StoredProcedure);

            return result;
        }

        /// See <see cref="IOrdersRepository"/> for more.
        public Task<OrderStatus> GetStatusByIdAsync(int id)
        {
            var inParams = new
            {
                id
            };

            Task<OrderStatus> result = _dbConnection.QuerySingleOrDefaultAsync<OrderStatus>("prc_OrderStatus_GetById", inParams, commandType: CommandType.StoredProcedure);

            return result;
        }

        /// See <see cref="IOrdersRepository"/> for more.
        public async Task<bool> UpdateStatus(int orderId, int statusId)
        {
            var inParams = new
            {
                orderId,
                statusId
            };

            int records = await _dbConnection.ExecuteAsync("prc_Orders_UpdateStatus", inParams, commandType: CommandType.StoredProcedure);

            return records > 0;
        }

        #endregion
    }
}
