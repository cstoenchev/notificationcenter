﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using NotificationCenter.Core.Repositories;

namespace NotificationCenter.Data.Repositories
{
    /// <summary>
    /// The users repository implementation.
    /// </summary>
    /// <seealso cref="IUsersRepository" />
    public class UsersRepository : IUsersRepository
    {
        #region Fields

        private readonly IDbConnection _dbConnection;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersRepository"/> class.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        public UsersRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        #endregion

        #region Public Methods

        /// See <see cref="IUsersRepository"/> for more.
        public Task<int?> GetUserIdByCredentialsAsync(string userName, string password)
        {
            var inParams = new
            {
                userName,
                password
            };

            Task<int?> result = _dbConnection.ExecuteScalarAsync<int?>("prc_Users_GetIdByCredentials", inParams, commandType: CommandType.StoredProcedure);

            return result;
        }

        /// See <see cref="IUsersRepository"/> for more.
        public Task<int?> GetUserIdByUserNameAsync(string userName)
        {
            var inParams = new
            {
                userName
            };

            Task<int?> result = _dbConnection.ExecuteScalarAsync<int?>("prc_Users_GetIdByUserName", inParams, commandType: CommandType.StoredProcedure);

            return result;
        }

        /// See <see cref="IUsersRepository"/> for more.
        public Task<int?> GetUserIdByClientIdAsync(int clientId)
        {
            var inParams = new
            {
                clientId
            };

            Task<int?> result = _dbConnection.QuerySingleOrDefaultAsync<int?>("prc_Users_GetIdByClientId", inParams, commandType: CommandType.StoredProcedure);

            return result;
        }

        #endregion
    }
}
