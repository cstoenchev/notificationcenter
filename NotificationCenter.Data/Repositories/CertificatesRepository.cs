﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Models.Domains.Certificates;

namespace NotificationCenter.Data.Repositories
{
    /// <summary>
    /// The certificates repository implementation.
    /// </summary>
    /// <seealso cref="ICertificatesRepository" />
    public class CertificatesRepository : ICertificatesRepository
    {
        #region Fields

        private readonly IDbConnection _dbConnection;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CertificatesRepository"/> class.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        public CertificatesRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        #endregion

        #region Public Methods

        /// See <see cref="ICertificatesRepository"/> for more.
        public Task<int> CreateAsync(Certificate certificate)
        {
            var inParams = new
            {
                certificate.Number,
                certificate.ValidFrom,
                certificate.ValidTo,
                certificate.ClientId
            };

            Task<int> result = _dbConnection.ExecuteScalarAsync<int>("prc_Certificates_Create", inParams, commandType: CommandType.StoredProcedure);

            return result;
        }

        /// See <see cref="ICertificatesRepository"/> for more.
        public Task<Certificate> GetByIdAsync(int id)
        {
            var inParams = new
            {
                id
            };

            Task<Certificate> result = _dbConnection.QuerySingleOrDefaultAsync<Certificate>("prc_Certificates_GetById", inParams, commandType: CommandType.StoredProcedure);

            return result;
        }

        #endregion
    }
}
