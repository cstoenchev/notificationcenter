﻿using System;

namespace NotificationCenter.Models.Domains.Certificates
{
    /// <summary>
    /// The certificate domain.
    /// </summary>
    public class Certificate
    {
        #region Constctors

        /// <summary>
        /// Initializes a new instance of the <see cref="Certificate"/> class.
        /// </summary>
        public Certificate(
            int id,
            string number,
            DateTime validFrom,
            DateTime validTo,
            int clientId)
        {
            Id = id;
            Number = number;
            ValidFrom = validFrom;
            ValidTo = validTo;
            ClientId = clientId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets the number.
        /// </summary>
        public string Number { get; }

        /// <summary>
        /// Gets the valid from.
        /// </summary>
        public DateTime ValidFrom { get; }

        /// <summary>
        /// Gets the valid to.
        /// </summary>
        public DateTime ValidTo { get; }

        /// <summary>
        /// Gets the client identifier.
        /// </summary>
        public int ClientId { get; }

        #endregion
    }
}