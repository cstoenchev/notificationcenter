﻿using NotificationCenter.Models.Domains.Clients;

namespace NotificationCenter.Models.Domains.Orders
{
    /// <summary>
    /// The order details.
    /// </summary>
    public class OrderDetails
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderDetails"/> class.
        /// </summary>
        public OrderDetails(
            int id,
            string number,
            int orderTypeId,
            string typeName,
            int orderStatusId,
            string statusName,
            int clientId,
            string clientFirstName,
            string clientLastName)
        {
            Id = id;
            Number = number;
            Type = new OrderType(orderTypeId, typeName);
            Status = new OrderStatus(orderStatusId, statusName);
            Client = new ClientBase(clientId, clientFirstName, clientLastName);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets the number.
        /// </summary>
        public string Number { get; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        public OrderType Type { get; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public OrderStatus Status { get; }

        /// <summary>
        /// Gets the client.
        /// </summary>
        public ClientBase Client { get; }

        #endregion
    }
}
