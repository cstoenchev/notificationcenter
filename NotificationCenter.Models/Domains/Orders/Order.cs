﻿namespace NotificationCenter.Models.Domains.Orders
{
    /// <summary>
    /// The order domain.
    /// </summary>
    public class Order
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Order"/> class.
        /// </summary>
        public Order(
            int id,
            string number,
            int orderTypeId,
            string typeName,
            int orderStatusId,
            string statusName,
            int userId)
        {
            Id = id;
            Number = number;
            Type = new OrderType(orderTypeId, typeName);
            Status = new OrderStatus(orderStatusId, statusName);
            UserId = userId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets the number.
        /// </summary>
        public string Number { get; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        public OrderType Type { get; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public OrderStatus Status { get; }

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        public int UserId { get; }

        #endregion
    }
}