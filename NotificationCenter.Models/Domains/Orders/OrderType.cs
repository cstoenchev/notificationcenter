﻿namespace NotificationCenter.Models.Domains.Orders
{
    /// <summary>
    /// The order type domain.
    /// </summary>
    public class OrderType
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderType"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        public OrderType(int id, string name)
        {
            Id = id;
            Name = name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; }

        #endregion
    }
}
