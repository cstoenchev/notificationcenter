﻿namespace NotificationCenter.Models.Domains.Orders
{
    /// <summary>
    /// The order status domain.
    /// </summary>
    public class OrderStatus
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderStatus"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        public OrderStatus(int id, string name)
        {
            Id = id;
            Name = name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; }

        #endregion
    }
}
