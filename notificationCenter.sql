USE [master]
GO
/****** Object:  Database [NotificationCenter]    Script Date: 27.2.2017 г. 11:31:58 ******/
CREATE DATABASE [NotificationCenter]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NotificationCenter', FILENAME = N'D:\Database\MSSQL12.LOCALINSTANCE\MSSQL\DATA\NotificationCenter.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'NotificationCenter_log', FILENAME = N'D:\Database\MSSQL12.LOCALINSTANCE\MSSQL\DATA\NotificationCenter_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [NotificationCenter] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NotificationCenter].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NotificationCenter] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NotificationCenter] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NotificationCenter] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NotificationCenter] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NotificationCenter] SET ARITHABORT OFF 
GO
ALTER DATABASE [NotificationCenter] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NotificationCenter] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NotificationCenter] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NotificationCenter] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NotificationCenter] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NotificationCenter] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NotificationCenter] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NotificationCenter] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NotificationCenter] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NotificationCenter] SET  DISABLE_BROKER 
GO
ALTER DATABASE [NotificationCenter] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NotificationCenter] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NotificationCenter] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [NotificationCenter] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NotificationCenter] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NotificationCenter] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NotificationCenter] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NotificationCenter] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [NotificationCenter] SET  MULTI_USER 
GO
ALTER DATABASE [NotificationCenter] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NotificationCenter] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NotificationCenter] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NotificationCenter] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [NotificationCenter] SET DELAYED_DURABILITY = DISABLED 
GO
USE [NotificationCenter]
GO
/****** Object:  Schema [HangFire]    Script Date: 27.2.2017 г. 11:31:58 ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  Table [dbo].[Certificate]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Certificate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](50) NOT NULL,
	[ClientId] [int] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
 CONSTRAINT [PK_Certificate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Client]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientTypeId] [int] NOT NULL,
	[Number] [nvarchar](20) NOT NULL,
	[FirstName] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClientType]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [int] NOT NULL,
 CONSTRAINT [PK_ClientType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[NotificationTypeId] [int] NOT NULL,
	[NotificationChannelId] [int] NOT NULL,
	[ClientTypeId] [int] NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationChannel]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationChannel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[Code] [int] NOT NULL,
 CONSTRAINT [PK_NotificationChannel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationType]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Code] [int] NULL,
 CONSTRAINT [PK_NotificationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](20) NOT NULL,
	[ClientId] [int] NOT NULL,
	[OrderTypeId] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_Order_CreateDate]  DEFAULT (getdate()),
	[OrderStatusId] [int] NOT NULL CONSTRAINT [DF_Order_OrderStatusId]  DEFAULT ((1)),
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Code] [int] NOT NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderType]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[Code] [int] NOT NULL,
 CONSTRAINT [PK_OrderType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClientId] [int] NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[Password] [varchar](40) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Client] ON 

INSERT [dbo].[Client] ([Id], [ClientTypeId], [Number], [FirstName], [LastName]) VALUES (3, 1, N'123', N'Чавдар', N'Стоенчев')
INSERT [dbo].[Client] ([Id], [ClientTypeId], [Number], [FirstName], [LastName]) VALUES (4, 2, N'456', N'Иван', N'Иванов')
SET IDENTITY_INSERT [dbo].[Client] OFF
SET IDENTITY_INSERT [dbo].[ClientType] ON 

INSERT [dbo].[ClientType] ([Id], [Name], [Code]) VALUES (1, N'Физическо лице', 1)
INSERT [dbo].[ClientType] ([Id], [Name], [Code]) VALUES (2, N'Юридическо лице', 2)
SET IDENTITY_INSERT [dbo].[ClientType] OFF
SET IDENTITY_INSERT [dbo].[Notification] ON 

INSERT [dbo].[Notification] ([Id], [Name], [NotificationTypeId], [NotificationChannelId], [ClientTypeId]) VALUES (1, N'Изтекъл сертификат', 1, 1, 1)
INSERT [dbo].[Notification] ([Id], [Name], [NotificationTypeId], [NotificationChannelId], [ClientTypeId]) VALUES (2, N'Промяна статус заявка', 2, 1, NULL)
SET IDENTITY_INSERT [dbo].[Notification] OFF
SET IDENTITY_INSERT [dbo].[NotificationChannel] ON 

INSERT [dbo].[NotificationChannel] ([Id], [Name], [Code]) VALUES (1, N'Web проложение', 1)
INSERT [dbo].[NotificationChannel] ([Id], [Name], [Code]) VALUES (2, N'Мобилно приложение', 2)
INSERT [dbo].[NotificationChannel] ([Id], [Name], [Code]) VALUES (3, N'Email', 3)
INSERT [dbo].[NotificationChannel] ([Id], [Name], [Code]) VALUES (4, N'SMS', 4)
SET IDENTITY_INSERT [dbo].[NotificationChannel] OFF
SET IDENTITY_INSERT [dbo].[NotificationType] ON 

INSERT [dbo].[NotificationType] ([Id], [Name], [Code]) VALUES (1, N'Изтекъл цифров сертификат', 1)
INSERT [dbo].[NotificationType] ([Id], [Name], [Code]) VALUES (2, N'Промяна статус на заявка', 2)
SET IDENTITY_INSERT [dbo].[NotificationType] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([Id], [Number], [ClientId], [OrderTypeId], [CreateDate], [OrderStatusId]) VALUES (7, N'123', 3, 1, CAST(N'2017-02-27 11:22:56.600' AS DateTime), 1)
INSERT [dbo].[Order] ([Id], [Number], [ClientId], [OrderTypeId], [CreateDate], [OrderStatusId]) VALUES (8, N'456', 3, 2, CAST(N'2017-02-27 11:23:04.737' AS DateTime), 1)
INSERT [dbo].[Order] ([Id], [Number], [ClientId], [OrderTypeId], [CreateDate], [OrderStatusId]) VALUES (9, N'789', 4, 1, CAST(N'2017-02-27 11:23:13.570' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Order] OFF
SET IDENTITY_INSERT [dbo].[OrderStatus] ON 

INSERT [dbo].[OrderStatus] ([Id], [Name], [Code]) VALUES (1, N'Въведена', 1)
INSERT [dbo].[OrderStatus] ([Id], [Name], [Code]) VALUES (2, N'Стокова разписка', 2)
INSERT [dbo].[OrderStatus] ([Id], [Name], [Code]) VALUES (3, N'Фактура', 3)
INSERT [dbo].[OrderStatus] ([Id], [Name], [Code]) VALUES (4, N'Получена', 4)
SET IDENTITY_INSERT [dbo].[OrderStatus] OFF
SET IDENTITY_INSERT [dbo].[OrderType] ON 

INSERT [dbo].[OrderType] ([Id], [Name], [Code]) VALUES (1, N'Покупка', 1)
INSERT [dbo].[OrderType] ([Id], [Name], [Code]) VALUES (2, N'Продажба', 1)
SET IDENTITY_INSERT [dbo].[OrderType] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [ClientId], [UserName], [Password]) VALUES (4, NULL, N'admin', N'admin')
INSERT [dbo].[User] ([Id], [ClientId], [UserName], [Password]) VALUES (7, 3, N'cstoenchev', N'chavdar')
INSERT [dbo].[User] ([Id], [ClientId], [UserName], [Password]) VALUES (8, 4, N'iivanov', N'ivan')
SET IDENTITY_INSERT [dbo].[User] OFF
/****** Object:  Index [IND_ClientId_Unique]    Script Date: 27.2.2017 г. 11:31:58 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IND_ClientId_Unique] ON [dbo].[Certificate]
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IND_ClientId_Unique]    Script Date: 27.2.2017 г. 11:31:58 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IND_ClientId_Unique] ON [dbo].[User]
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Certificate]  WITH CHECK ADD  CONSTRAINT [FK_Certificate_Client_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Certificate] CHECK CONSTRAINT [FK_Certificate_Client_ClientId]
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_ClientType_ClientTypeId] FOREIGN KEY([ClientTypeId])
REFERENCES [dbo].[ClientType] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_ClientType_ClientTypeId]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_ClientType_ClientTypeId] FOREIGN KEY([ClientTypeId])
REFERENCES [dbo].[ClientType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_ClientType_ClientTypeId]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_NotificationChannel_NotificationChannelId] FOREIGN KEY([NotificationChannelId])
REFERENCES [dbo].[NotificationChannel] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_NotificationChannel_NotificationChannelId]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_NotificationType_NotificationTypeId] FOREIGN KEY([NotificationTypeId])
REFERENCES [dbo].[NotificationType] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_NotificationType_NotificationTypeId]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Client_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Client_ClientId]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderStatus_OrderStatusId] FOREIGN KEY([OrderStatusId])
REFERENCES [dbo].[OrderStatus] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderStatus_OrderStatusId]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderType_OrderTypeId] FOREIGN KEY([OrderTypeId])
REFERENCES [dbo].[OrderType] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderType_OrderTypeId]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Client_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Client] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Client_ClientId]
GO
/****** Object:  StoredProcedure [dbo].[prc_Certificates_Create]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Certificates_Create]
	@Number NVARCHAR(50),
	@ValidFrom DATETIME,
	@ValidTo DATETIME,
	@ClientId INT
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO [Certificate] (Number, ValidFrom, ValidTo, ClientId)
                     VALUES (@Number, @ValidFrom, @ValidTo, @ClientId)

  SELECT SCOPE_IDENTITY();
END

GO
/****** Object:  StoredProcedure [dbo].[prc_Certificates_GetById]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Certificates_GetById]
    @Id INT
AS
BEGIN
  SET NOCOUNT ON;

  SELECT c.Id,
         c.Number,
		 c.ValidFrom,
		 c.ValidTo,
		 c.ClientId
   FROM [Certificate] c
  WHERE c.Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[prc_Clients_GetAll]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Clients_GetAll]
AS
BEGIN
  SET NOCOUNT ON;

  SELECT c.Id,
         c.FirstName,
		 c.LastName
   FROM [Client] c
END

GO
/****** Object:  StoredProcedure [dbo].[prc_Orders_GetById]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Orders_GetById]
    @Id int
AS
BEGIN
  SET NOCOUNT ON;

  SELECT o.Id,
         o.Number,
		 o.OrderTypeId,
		 ot.Name as TypeName,
		 o.OrderStatusId,
		 os.Name as StatusName,
		 u.Id as UserId
    FROM [Order] o 
	JOIN [OrderType] ot ON o.OrderTypeId = ot.Id
	JOIN [OrderStatus] os ON o.OrderStatusId = os.Id
	JOIN [User] u ON o.ClientId = u.ClientId
   WHERE o.Id = @Id
   ORDER BY o.CreateDate
END

GO
/****** Object:  StoredProcedure [dbo].[prc_Orders_GetForUser]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Orders_GetForUser]
    @UserId int
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @ClientId INT;

  SELECT @ClientId = u.ClientId
    FROM [User] u
   WHERE u.Id = @UserId

  SELECT o.Id,
         o.Number,
		 o.OrderTypeId,
		 ot.Name as TypeName,
		 o.OrderStatusId,
		 os.Name as StatusName,
		 o.ClientId,
		 c.FirstName as ClientFirstName,
		 c.LastName as ClientLastName
    FROM [Order] o 
	JOIN [OrderType] ot ON o.OrderTypeId = ot.Id
	JOIN [OrderStatus] os ON o.OrderStatusId = os.Id
	JOIN [Client] c ON o.ClientId = c.Id
   WHERE ISNULL(@ClientId, o.ClientId) = o.ClientId
   ORDER BY o.CreateDate
END

GO
/****** Object:  StoredProcedure [dbo].[prc_Orders_UpdateStatus]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Orders_UpdateStatus]
    @OrderId int,
	@StatusId int
AS
BEGIN
  UPDATE [Order]
     SET OrderStatusId = @StatusId
   WHERE Id = @OrderId
END

GO
/****** Object:  StoredProcedure [dbo].[prc_OrderStatus_GetById]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_OrderStatus_GetById]
    @Id int
AS
BEGIN
  SET NOCOUNT ON;

  SELECT os.Id, os.Name
    FROM OrderStatus os
   WHERE os.Id = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[prc_Users_GetIdByClientId]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Users_GetIdByClientId]
    @ClientId INT
AS
BEGIN
  SET NOCOUNT ON;

  SELECT u.Id
    FROM [User] u
   WHERE u.ClientId = @ClientId
END

GO
/****** Object:  StoredProcedure [dbo].[prc_Users_GetIdByCredentials]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Users_GetIdByCredentials]
    @UserName NVARCHAR(20),
	@Password VARCHAR(40)
AS
BEGIN
  SET NOCOUNT ON;

  SELECT u.Id
    FROM [User] u
   WHERE u.UserName = @UserName AND u.Password = @Password
END

GO
/****** Object:  StoredProcedure [dbo].[prc_Users_GetIdByUserName]    Script Date: 27.2.2017 г. 11:31:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[prc_Users_GetIdByUserName]
    @UserName NVARCHAR(20)
AS
BEGIN
  SET NOCOUNT ON;

  SELECT u.Id
    FROM [User] u
   WHERE u.UserName = @UserName
END

GO
USE [master]
GO
ALTER DATABASE [NotificationCenter] SET  READ_WRITE 
GO
