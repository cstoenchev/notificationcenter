﻿$(function () {
    var alertEl = 
        '<div class="alert alert-info alert-dismissible" role="alert">' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        ' {{message}} '+
        '</div>';

    var notificationHub = $.connection.notificationHub;

    notificationHub.client.sendUserMessage = function (message) {
        var alert = alertEl.replace('{{message}}', message);

        $('.body-content').prepend($(alert));
    }

    $.connection.hub.start();
});