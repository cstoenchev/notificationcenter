﻿jQuery(function ($) {
    var $allforms = $('form');

    $allforms.find('.validation-summary-errors').addClass('alert alert-danger');

    $allforms.find('div.form-group').each(function () {
        var $this = $(this);

        $this.toggleClass('has-error', $this.find('span.field-validation-error').length > 0)
    });

    $('form').on('submit', function () {
        var $form = $(this);

        if (!$form.valid()) {
            $form.find('div.form-group').each(function () {
                var $this = $(this);
                $this.addClass('has-error', $this.find('span.field-validation-error').length > 0)
            });

            $form.find('.validation-summary-errors').addClass('alert alert-danger');
        }
        else {
            $form.find('div.form-group').removeClass('has-error');
            $form.find('.validation-summary-valid, .validation-summary-errors').removeClass('alert alert-danger');
            $form.find('.validation-summary-valid > ul, .validation-summary-errors > ul').html('');
        }
    });

    $('form').on('reset', function () {
        var $form = $(this);

        $form.find('div.form-group').removeClass('has-error');
        $form.find('.validation-summary-valid, .validation-summary-errors').removeClass('alert alert-danger');
        $form.find('.validation-summary-valid > ul, .validation-summary-errors > ul').html('');
    });

    // Update the validator highlight and unhighlight handlers.
    $.validator.setDefaults({
        highlight: function (element) {
            $(element).closest(".form-group").addClass("has-error");
        },
        unhighlight: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });

    // Update the validator date validation.
    $.validator.methods.date = function (value, element) {
        var datePicker = $(element).data('DateTimePicker');
        if (!datePicker) {
            datePicker = $(element).closest('.input-group.date').data('DateTimePicker');
        }

        if (!datePicker) {
            return true;
        }

        return datePicker.getMoment().isValid();
    };
}(jQuery));