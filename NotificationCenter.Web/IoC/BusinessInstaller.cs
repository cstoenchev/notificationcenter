﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using NotificationCenter.Business.Services;
using NotificationCenter.Core.Services;

namespace NotificationCenter.Web.IoC
{
    /// <summary>
    /// The business dependency installer.
    /// </summary>
    /// <seealso cref="IWindsorInstaller" />
    public class BusinessInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IUsersService>()
                .ImplementedBy<UsersService>()
                .LifeStyle.HybridPerWebRequestTransient());

            container.Register(
                Component.For<IOrdersService>()
                .ImplementedBy<OrdersService>()
                .LifeStyle.HybridPerWebRequestTransient());

            container.Register(
                Component.For<ICertificatesService>()
                .ImplementedBy<CertificatesService>()
                .LifeStyle.HybridPerWebRequestTransient());

            container.Register(
                Component.For<IClientsService>()
                .ImplementedBy<ClientsService>()
                .LifeStyle.HybridPerWebRequestTransient());
        }
    }
}