﻿using System.Web;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Security;
using NotificationCenter.Core.Providers;
using NotificationCenter.Web.Providers;
using NotificationCenter.Web.SignalR.Providers;

namespace NotificationCenter.Web.IoC
{
    /// <summary>
    /// The web dependency installer.
    /// </summary>
    /// <seealso cref="IWindsorInstaller" />
    public class WebInstaller : IWindsorInstaller
    {
        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IUserIdProvider>()
                .ImplementedBy<UserIdProvider>()
                .LifestylePerWebRequest());

            container.Register(
                Component.For<INotificationProvider>()
                .ImplementedBy<NotificationProvider>()
                .LifeStyle.HybridPerWebRequestTransient());

            container.Register(
                Component.For<IAuthenticationManager>()
                .UsingFactoryMethod((kernel, creationContext) =>
                {
                    return HttpContext.Current.GetOwinContext().Authentication;
                })
                .LifestylePerWebRequest());
        }
    }
}