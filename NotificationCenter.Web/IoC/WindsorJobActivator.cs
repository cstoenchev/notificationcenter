﻿using System;
using Castle.MicroKernel;
using Hangfire;

namespace NotificationCenter.Web.IoC
{
    /// <summary>
    /// The windsor job activator.
    /// </summary>
    /// <seealso cref="JobActivator" />
    public class WindsorJobActivator : JobActivator
    {
        #region Fields

        private readonly IKernel _kernel;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WindsorJobActivator"/> class.
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        public WindsorJobActivator(IKernel kernel)
        {
            _kernel = kernel;
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Activates the hangfire job.
        /// </summary>
        /// <param name="jobType">Type of the job.</param>
        public override object ActivateJob(Type jobType)
        {
            return _kernel.Resolve(jobType);
        }

        #endregion
    }
}