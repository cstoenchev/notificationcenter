﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel;
using Microsoft.AspNet.SignalR;

namespace NotificationCenter.Web.IoC
{
    /// <summary>
    /// The windsor dependecy resolver.
    /// </summary>
    /// <seealso cref="DefaultDependencyResolver" />
    public class WindsorDependecyResolver : DefaultDependencyResolver
    {
        #region Fields

        private readonly IKernel _kernel;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WindsorDependecyResolver"/> class.
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        public WindsorDependecyResolver(IKernel kernel)
        {
            _kernel = kernel;
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        public override object GetService(Type serviceType)
        {
            return _kernel.HasComponent(serviceType)
                ? _kernel.Resolve(serviceType)
                : base.GetService(serviceType);
        }

        /// <summary>
        /// Gets the services.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        public override IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.HasComponent(serviceType)
                ? _kernel.ResolveAll(serviceType).Cast<object>()
                : base.GetServices(serviceType);
        }

        #endregion
    }
}