﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Data.Repositories;

namespace NotificationCenter.Web.IoC
{
    /// <summary>
    /// The data dependency installer.
    /// </summary>
    /// <seealso cref="IWindsorInstaller" />
    public class DataInstaller : IWindsorInstaller
    {
        #region Fields

        private readonly string _connectionString;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataInstaller"/> class.
        /// </summary>
        public DataInstaller()
        {
            // Save connection string so we do not read every time from the disc when require connection.
            _connectionString = ConfigurationManager.ConnectionStrings["NotificationCenter"].ConnectionString;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IDbConnection>()
                .UsingFactoryMethod((kernel) =>
                {
                    return new SqlConnection(_connectionString);
                })
                .LifeStyle.HybridPerWebRequestTransient());

            container.Register(
                Component.For<IUsersRepository>()
                .ImplementedBy<UsersRepository>()
                .LifeStyle.HybridPerWebRequestTransient());

            container.Register(
                Component.For<IOrdersRepository>()
                .ImplementedBy<OrdersRepository>()
                .LifeStyle.HybridPerWebRequestTransient());

            container.Register(
                Component.For<ICertificatesRepository>()
                .ImplementedBy<CertificatesRepository>()
                .LifeStyle.HybridPerWebRequestTransient());

            container.Register(
                Component.For<IClientsRepository>()
                .ImplementedBy<ClientsRepository>()
                .LifeStyle.HybridPerWebRequestTransient());
        }

        #endregion
    }
}