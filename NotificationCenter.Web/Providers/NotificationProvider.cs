﻿using System;
using System.Threading.Tasks;
using Hangfire;
using NotificationCenter.Core.Providers;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Models.Domains.Certificates;
using NotificationCenter.Models.Domains.Orders;
using NotificationCenter.Web.SignalR;

namespace NotificationCenter.Web.Providers
{
    /// <summary>
    /// The notification provider implementation.
    /// </summary>
    /// <seealso cref="INotificationProvider" />
    public class NotificationProvider : INotificationProvider
    {
        #region Fields

        private readonly IUsersRepository _usersRepository;
        private readonly ICertificatesRepository _certificatesRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationProvider"/> class.
        /// </summary>
        public NotificationProvider(
            IUsersRepository usersRepository,
            ICertificatesRepository certificatesRepository)
        {
            _usersRepository = usersRepository;
            _certificatesRepository = certificatesRepository;
        }

        #endregion

        #region Public Methods

        /// See <see cref="INotificationProvider"/> for more.
        public Task NotifyChangedOrderStatusAsync(Order order, OrderStatus newStatus)
        {
            // TODO : Check enabled notification.
            // TODO : If client can edit self order status do not send notification to him.

            string message = $"Статусът на поръчка с номер {order.Number} и тип {order.Type.Name} беше променен от {order.Status.Name} на {newStatus.Name}.";

            NotificationHub.SendUserNotification(order.UserId, message);

            return Task.CompletedTask;
        }

        /// See <see cref="INotificationProvider"/> for more.
        public async Task NotifyExpiredCertificateAsync(int id)
        {
            // TODO : Check for enabled notification.

            Certificate certificate = await _certificatesRepository.GetByIdAsync(id);

            if (certificate == null)
            {
                return;
            }

            int? userId = await _usersRepository.GetUserIdByClientIdAsync(certificate.ClientId);

            if (!userId.HasValue)
            {
                return;
            }

            string message = $"Сертификат {certificate.Number} валиден от {certificate.ValidFrom} до {certificate.ValidTo} изтече.";

            NotificationHub.SendUserNotification(userId.Value, message);
        }

        /// See <see cref="INotificationProvider"/> for more.
        public Task RegisterNotificationExpiredCertificateAsync(int id, DateTime expiresAt)
        {
            BackgroundJob.Schedule<INotificationProvider>(provider => provider.NotifyExpiredCertificateAsync(id), expiresAt);

            return Task.CompletedTask;
        }

        #endregion
    }
}