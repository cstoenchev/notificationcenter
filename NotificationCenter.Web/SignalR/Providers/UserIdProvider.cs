﻿using Microsoft.AspNet.SignalR;
using NotificationCenter.Core.Services;

namespace NotificationCenter.Web.SignalR.Providers
{
    /// <summary>
    /// The user identifier provider implementation.
    /// </summary>
    /// <seealso cref="IUserIdProvider" />
    public class UserIdProvider : IUserIdProvider
    {
        #region Fields

        private readonly IUsersService _usersService;

        #endregion

        #region Constructors

        public UserIdProvider(IUsersService usersService)
        {
            _usersService = usersService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        public string GetUserId(IRequest request)
        {
            int? userId = _usersService.GetUserIdByUserNameAsync(request.User.Identity.Name).ConfigureAwait(false).GetAwaiter().GetResult();

            return userId.ToString();
        }

        #endregion
    }
}