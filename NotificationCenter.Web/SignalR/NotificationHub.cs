﻿using Microsoft.AspNet.SignalR;

namespace NotificationCenter.Web.SignalR
{
    /// <summary>
    /// The notification hub.
    /// </summary>
    /// <seealso cref="Hub" />
    public class NotificationHub : Hub
    {
        public static void SendUserNotification(int userId, string message)
        {
            var instance = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();

            instance.Clients.User(userId.ToString()).sendUserMessage(message);
        }
    }
}