﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using NotificationCenter.Core.Services;
using NotificationCenter.Web.Models.Accounts;

namespace NotificationCenter.Web.Controllers
{
    /// <summary>
    /// The accounts controller.
    /// </summary>
    /// <seealso cref="Controller" />
    public class AccountsController : Controller
    {
        #region Fields

        private readonly IUsersService _usersService;
        private readonly IAuthenticationManager _authenticationManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsController"/> class.
        /// </summary>
        public AccountsController(
            IUsersService usersService,
            IAuthenticationManager authenticationManager)
        {
            _usersService = usersService;
            _authenticationManager = authenticationManager;
        }

        #endregion

        #region Actions

        /// <summary>
        /// The login GET action.
        /// </summary>
        [HttpGet]
        public ActionResult Login()
        {
            var viewModel = new LoginViewModel();

            return View(viewModel);
        }

        /// <summary>
        /// The login POST action.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        [HttpPost]
        [ActionName("Login")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LoginAsync(LoginViewModel viewModel, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            // TODO : Make password with hash and salt.
            int? userId = await _usersService.GetUserIdByCredentialsAsync(viewModel.UserName, viewModel.Password);

            if (!userId.HasValue)
            {
                ModelState.AddModelError(nameof(LoginViewModel.UserName), "Invalid username or password.");

                return View(viewModel);
            }

            var authProperties = new AuthenticationProperties
            {
                IsPersistent = viewModel.RememberMe
            };

            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);

            identity.AddClaim(new Claim(ClaimTypes.Sid, userId.Value.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, viewModel.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Name, viewModel.UserName));

            _authenticationManager.SignIn(authProperties, identity);

            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Logout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Login");
        }

        #endregion
    }
}