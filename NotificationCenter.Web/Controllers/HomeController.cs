﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using NotificationCenter.Core.Services;
using NotificationCenter.Models.Domains.Orders;
using NotificationCenter.Web.Extensions;

namespace NotificationCenter.Web.Controllers
{
    /// <summary>
    /// The home controller.
    /// </summary>
    /// <seealso cref="Controller" />
    [Authorize]
    public class HomeController : Controller
    {
        #region Fields

        private readonly IOrdersService _ordersService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="ordersService">The orders service.</param>
        public HomeController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        #endregion

        /// <summary>
        /// Index GET action.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            int userId = User.GetId();

            IEnumerable<OrderDetails> orders = await _ordersService.GetForUserAsync(userId);

            return View(orders);
        }
    }
}