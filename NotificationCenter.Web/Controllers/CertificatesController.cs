﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using NotificationCenter.Core.Services;
using NotificationCenter.Models.Domains.Certificates;
using NotificationCenter.Web.Models.Certificates;

namespace NotificationCenter.Web.Controllers
{
    /// <summary>
    /// The certificates controller.
    /// </summary>
    /// <seealso cref="Controller" />
    [Authorize]
    public class CertificatesController : Controller
    {
        #region Fields

        private readonly ICertificatesService _certificatesService;
        private readonly IClientsService _clientsService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CertificatesController"/> class.
        /// </summary>
        /// <param name="certificatesService">The certificates service.</param>
        public CertificatesController(
            ICertificatesService certificatesService,
            IClientsService clientsService)
        {
            _certificatesService = certificatesService;
            _clientsService = clientsService;
        }

        #endregion

        /// <summary>
        /// Creates certificate. Get Action.
        /// </summary>
        [HttpGet]
        [ActionName("Create")]
        public async Task<ActionResult> CreateAsync()
        {
            var viewModel = new CertificateViewModel();

            viewModel.Clients = await _clientsService.GetAllAsync();

            return View(viewModel);
        }

        /// <summary>
        /// Creates certificate. POST Action.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Create")]
        public async Task<ActionResult> CreateAsync(CertificateViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel.Clients = await _clientsService.GetAllAsync();

                return View(viewModel);
            }

            // For testing (demonstrating) purposes.
            DateTime validTo = DateTime.Now.AddMinutes(2);

            var domain = new Certificate(0, viewModel.Number, viewModel.ValidFrom.Value, validTo, viewModel.ClientId.Value);

            int id = await _certificatesService.CreateAsync(domain);

            return RedirectToAction("Index", "Home");
        }
    }
}