﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using NotificationCenter.Core.Services;
using NotificationCenter.Web.Models.Orders;

namespace NotificationCenter.Web.Controllers
{
    /// <summary>
    /// The orders controller.
    /// </summary>
    /// <seealso cref="Controller" />
    [Authorize]
    public class OrdersController : Controller
    {
        #region Fields

        private readonly IOrdersService _ordersService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersController"/> class.
        /// </summary>
        /// <param name="ordersService">The orders service.</param>
        public OrdersController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Updates the status. PUT Action.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="viewModel">The view model.</param>
        [HttpPost]
        public async Task<ActionResult> UpdateStatus(OrderStatusViewModel viewModel)
        {
            bool isSuccess = await _ordersService.UpdateStatus(viewModel.OrderId, viewModel.StatusId);

            if (!isSuccess)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion
    }
}