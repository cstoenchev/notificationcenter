﻿namespace NotificationCenter.Web.Models.Orders
{
    /// <summary>
    /// The order status view model.
    /// </summary>
    public class OrderStatusViewModel
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets the status identifier.
        /// </summary>
        public int StatusId { get; set; }
    }
}