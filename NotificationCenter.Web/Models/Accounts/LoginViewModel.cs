﻿using System.ComponentModel.DataAnnotations;

namespace NotificationCenter.Web.Models.Accounts
{
    /// <summary>
    /// The login view model.
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        [Required(ErrorMessage = "Please enter user name.")]
        [MaxLength(20, ErrorMessage = "The maximum length of user name must be {1}.")]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required(ErrorMessage = "Please enter password.")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether authentication cookie is persistent.
        /// </summary>
        public bool RememberMe { get; set; }
    }
}