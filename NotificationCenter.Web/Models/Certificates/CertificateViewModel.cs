﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using NotificationCenter.Models.Domains.Clients;

namespace NotificationCenter.Web.Models.Certificates
{
    /// <summary>
    /// The certificate view model.
    /// </summary>
    public class CertificateViewModel
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Number { get; set; }

        /// <summary>
        /// Gets or sets the valid from.
        /// </summary>
        [Required]
        public DateTime? ValidFrom { get; set; }

        /// <summary>
        /// Gets or sets the valid to.
        /// </summary>
        [Required]
        public DateTime? ValidTo { get; set; }

        /// <summary>
        /// Gets or sets the client identifier.
        /// </summary>
        [Required]
        public int? ClientId { get; set; }

        /// <summary>
        /// Gets or sets the clients.
        /// </summary>
        public IEnumerable<ClientBase> Clients { get; set; }
    }
}