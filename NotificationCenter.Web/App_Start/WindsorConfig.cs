﻿using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Hangfire;
using Microsoft.AspNet.SignalR;
using NotificationCenter.Web.IoC;

namespace NotificationCenter.Web
{
    /// <summary>
    /// The dependency injection configuration.
    /// </summary>
    public static class WindsorConfig
    {
        #region Private Fields

        private static IWindsorContainer _container;

        #endregion

        #region Public Methods

        /// <summary>
        /// Setups the windsor configuration.
        /// </summary>
        public static void Setup()
        {
            _container = new WindsorContainer().Install(FromAssembly.This());

            WindsorControllerFactory controllerFactory = new WindsorControllerFactory(_container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            WindsorDependecyResolver dependencyResolver = new WindsorDependecyResolver(_container.Kernel);
            GlobalHost.DependencyResolver = dependencyResolver;

            WindsorJobActivator jobActivator = new WindsorJobActivator(_container.Kernel);
            GlobalConfiguration.Configuration.UseActivator(jobActivator);
        }

        /// <summary>
        /// Releases this instance.
        /// </summary>
        public static void Release()
        {
            _container.Dispose();
        }

        /// <summary>
        /// Resolves this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        #endregion
    }
}