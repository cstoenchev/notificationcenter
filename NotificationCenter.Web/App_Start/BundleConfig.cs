﻿using System.Web.Optimization;

namespace NotificationCenter.Web
{
    /// <summary>
    /// The bundle configuration.
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// Registers the bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
        }
    }
}
