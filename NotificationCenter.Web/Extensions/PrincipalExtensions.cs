﻿using System.Security.Claims;
using System.Security.Principal;

namespace NotificationCenter.Web.Extensions
{
    /// <summary>
    /// The principal extensions.
    /// </summary>
    public static class PrincipalExtensions
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public static int GetId(this IPrincipal principal)
        {
            var claimsPrincipal = principal as ClaimsPrincipal;

            if (claimsPrincipal == null)
            {
                return 0;
            }

            Claim subjectClaim = claimsPrincipal.FindFirst(ClaimTypes.Sid);

            if (subjectClaim == null)
            {
                return 0;
            }

            int userId;

            int.TryParse(subjectClaim.Value, out userId);

            return userId;
        }
    }
}