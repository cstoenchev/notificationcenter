﻿using Microsoft.Owin;

namespace NotificationCenter.Web.Extensions
{
    /// <summary>
    /// The owin request extensions.
    /// </summary>
    public static class OwinRequestExtensions
    {
        /// <summary>
        /// Determines whether request [is ajax request].
        /// </summary>
        /// <param name="request">The request.</param>
        public static bool IsAjaxRequest(this IOwinRequest request)
        {
            IReadableStringCollection query = request.Query;

            if (query != null)
            {
                if (query["X-Requested-With"] == "XMLHttpRequest")
                {
                    return true;
                }
            }

            IHeaderDictionary headers = request.Headers;

            if (headers != null)
            {
                if (headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return true;
                }
            }

            return false;
        }
    }
}