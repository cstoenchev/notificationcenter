﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NotificationCenter.Core.Providers;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Core.Services;
using NotificationCenter.Models.Domains.Orders;

namespace NotificationCenter.Business.Services
{
    /// <summary>
    /// The orders service implementation.
    /// </summary>
    /// <seealso cref="IOrdersService" />
    public class OrdersService : IOrdersService
    {
        #region Fields

        private readonly IOrdersRepository _ordersRepository;
        private readonly INotificationProvider _notificationProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersService" /> class.
        /// </summary>
        /// <param name="ordersRepository">The orders repository.</param>
        /// <param name="notificationProvider">The notification provider.</param>
        public OrdersService(
            IOrdersRepository ordersRepository,
            INotificationProvider notificationProvider)
        {
            _ordersRepository = ordersRepository;
            _notificationProvider = notificationProvider;
        }

        #endregion

        #region Public Methods

        /// See <see cref="IOrdersService"/> for more.
        public Task<IEnumerable<OrderDetails>> GetForUserAsync(int userId)
        {
            Task<IEnumerable<OrderDetails>> result = _ordersRepository.GetForUserAsync(userId);

            return result;
        }

        /// See <see cref="IOrdersService"/> for more.
        public Task<Order> GetByIdAsync(int id)
        {
            Task<Order> result = _ordersRepository.GetByIdAsync(id);

            return result;
        }

        /// See <see cref="IOrdersService"/> for more.
        public Task<OrderStatus> GetStatusByIdAsync(int id)
        {
            Task<OrderStatus> result = _ordersRepository.GetStatusByIdAsync(id);

            return result;
        }

        /// See <see cref="IOrdersService"/> for more.
        public async Task<bool> UpdateStatus(int orderId, int statusId)
        {
            Order order = await _ordersRepository.GetByIdAsync(orderId);

            bool isSuccess = await _ordersRepository.UpdateStatus(orderId, statusId);

            if (isSuccess)
            {
                OrderStatus orderStatus = await _ordersRepository.GetStatusByIdAsync(statusId);

                await _notificationProvider.NotifyChangedOrderStatusAsync(order, orderStatus);
            }

            return isSuccess;
        }

        #endregion
    }
}