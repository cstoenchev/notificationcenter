﻿using System.Threading.Tasks;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Core.Services;

namespace NotificationCenter.Business.Services
{
    /// <summary>
    /// The users service implementation.
    /// </summary>
    /// <seealso cref="IUsersService" />
    public class UsersService : IUsersService
    {
        #region Fields

        private readonly IUsersRepository _usersRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersService"/> class.
        /// </summary>
        /// <param name="usersRepository">The users repository.</param>
        public UsersService(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        #endregion

        /// See <see cref="IUsersService"/> for more.
        public Task<int?> GetUserIdByCredentialsAsync(string userName, string password)
        {
            Task<int?> result = _usersRepository.GetUserIdByCredentialsAsync(userName, password);

            return result;
        }

        /// See <see cref="IUsersService"/> for more.
        public Task<int?> GetUserIdByUserNameAsync(string userName)
        {
            Task<int?> result = _usersRepository.GetUserIdByUserNameAsync(userName);

            return result;
        }

        /// See <see cref="IUsersService"/> for more.
        public Task<int?> GetUserIdByClientIdAsync(int clientId)
        {
            Task<int?> result = _usersRepository.GetUserIdByClientIdAsync(clientId);

            return result;
        }
    }
}
