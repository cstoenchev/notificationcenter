﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Core.Services;
using NotificationCenter.Models.Domains.Clients;

namespace NotificationCenter.Business.Services
{
    /// <summary>
    /// The clients service implementation.
    /// </summary>
    /// <seealso cref="IClientsService" />
    public class ClientsService : IClientsService
    {
        #region Fields

        private readonly IClientsRepository _clientsRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientsService"/> class.
        /// </summary>
        public ClientsService(IClientsRepository clientsRepository)
        {
            _clientsRepository = clientsRepository;
        }

        #endregion

        #region Public Methods

        /// See <see cref="IClientsService"/> for more.
        public Task<IEnumerable<ClientBase>> GetAllAsync()
        {
            Task<IEnumerable<ClientBase>> result = _clientsRepository.GetAllAsync();

            return result;
        }

        #endregion
    }
}
