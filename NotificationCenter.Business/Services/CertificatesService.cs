﻿using System.Threading.Tasks;
using NotificationCenter.Core.Providers;
using NotificationCenter.Core.Repositories;
using NotificationCenter.Core.Services;
using NotificationCenter.Models.Domains.Certificates;

namespace NotificationCenter.Business.Services
{
    /// <summary>
    /// The certificates service implementation.
    /// </summary>
    /// <seealso cref="ICertificatesService" />
    public class CertificatesService : ICertificatesService
    {
        #region Fields

        private readonly ICertificatesRepository _certificatesRepository;
        private readonly INotificationProvider _notificationProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="CertificatesService"/> class.
        /// </summary>
        public CertificatesService(
            ICertificatesRepository certificatesRepository,
            INotificationProvider notificationProvider)
        {
            _certificatesRepository = certificatesRepository;
            _notificationProvider = notificationProvider;

        }

        #endregion

        #region Public Methods

        /// See <see cref="ICertificatesService"/> for more.
        public async Task<int> CreateAsync(Certificate certificate)
        {
            int id = await _certificatesRepository.CreateAsync(certificate);

            if (id > 0)
            {
                await _notificationProvider.RegisterNotificationExpiredCertificateAsync(id, certificate.ValidTo);
            }

            return id;
        }

        /// See <see cref="ICertificatesService"/> for more.
        public Task<Certificate> GetByIdAsync(int id)
        {
            Task<Certificate> result = _certificatesRepository.GetByIdAsync(id);

            return result;
        }

        #endregion
    }
}
